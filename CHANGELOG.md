# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.3.1"></a>
## [0.3.1](https://github.com/pixelass/beneath-the-sun/compare/v0.3.0...v0.3.1) (2016-08-12)


### Bug Fixes

* **string:** better sting handling ([c735ac7](https://github.com/pixelass/beneath-the-sun/commit/c735ac7))



<a name="0.3.0"></a>
# [0.3.0](https://github.com/pixelass/beneath-the-sun/compare/v0.2.1...v0.3.0) (2016-08-11)


### Features

* **postcss:** update plugin ([d00528c](https://github.com/pixelass/beneath-the-sun/commit/d00528c))



<a name="0.2.1"></a>
## [0.2.1](https://github.com/pixelass/beneath-the-sun/compare/v0.2.0...v0.2.1) (2016-08-11)



<a name="0.2.0"></a>
# [0.2.0](https://github.com/pixelass/beneath-the-sun/compare/v0.1.0...v0.2.0) (2016-08-11)


### Features

* **postcss:** added above and below ([99f20ed](https://github.com/pixelass/beneath-the-sun/commit/99f20ed))



<a name="0.1.0"></a>
# [0.1.0](https://github.com/pixelass/beneath-the-sun/compare/v0.0.2...v0.1.0) (2016-08-11)


### Features

* **postcss:** added postcss plugin ([3d5ebbf](https://github.com/pixelass/beneath-the-sun/commit/3d5ebbf))



<a name="0.0.2"></a>
## 0.0.2 (2016-08-11)
