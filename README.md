# Beneath the sun
postcss and SCSS library for z-index handling

[![Beneath the sun logo](https://raw.githubusercontent.com/pixelass/beneath-the-sun/master/beneath-the-sun.png)](https://github.com/pixelass/beneath-the-sun)

## g-index

a small collection of functions and mixins to handle your elements’ `z-index`. 
`g-index` uses "G"eographical names to make it very clear where the element will be placed. 

## npm

```shell
npm install --save-dev beneath-the-sun
```

## bower

```bower
bower install beneath-the-sun
```

## Demo

[Codepen demo](http://codepen.io/pixelass/pen/yJGjYr)

## Names

```scss
'underground' 'sea'   'land' 'hill'
 = -3          = 0     = 3     = 6
'mountain'    'cloud' 'moon' 'sun'
 = 9           = 12    = 15   = 18
```

There is no reason to use insane numbers like 999999999 when you know the limits. The steps between the values can be adjusted. The default value is 3
```scss
$g-index-levels: ( 'underground': -1,
                   'sea': 0,
                   'land': 1,
                   'hill': 10,
                   'mountain': 30,
                   'cloud': 50,
                   'moon': 100,
                   'sun': 200
                 ) ;
```
You can change the names too
```scss
$g-index-levels: ( 'below': -1,
                   'zero': 0,
                   'above': 1,
                   'fixed': 2,
                   'modal': 10
                 ) ;
.tooltip {
  z-index: g-index('above');
}
.dialog {
  z-index: g-index('modal');
}
.dialog-backdrop {
  z-index: g-below('modal');
}
```

You can also add your own debugging method. If debugging is enabled the elments will be translated on the z-axis to show the layers in 3-d space

## postcss

> for more options look at [postcss-g-index](https://github.com/pixelass/postcss-g-index)

```scss
.tooltip {
  z-index: g-index('land');
}
.dialog {
  z-index: g-index('mountain');
}
.dialog-backdrop {
  z-index: g-below('mountain');
}
```

