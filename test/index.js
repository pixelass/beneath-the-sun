const sass = require('node-sass')
const fs = require('fs')
const postcss = require('postcss')
const colors = require('colors')
const gIndex = require('../')

const dir = './test'

const file = `${dir}/test.scss`
const expected = `${dir}/expected.css`
const outFile = `${dir}/result.css`

const filePost = `${dir}/postcss-test.css`
const expectedPost = `${dir}/postcss-expected.css`
const outFilePost = `${dir}/postcss-result.css`

function xPect(testType, xpct, rslt) {
  fs.readFile(xpct,'utf-8',(err, data)=> {
    if (err) {
      throw err
    } else {
      const xpctd = data
      if (typeof rslt === 'string') {
        fs.readFile(rslt,'utf-8',(err, data)=> {
          if (err) {
            throw err
          } else {
            if (xpctd === data) {
              console.log(`👍  ${testType} test passed`.green)
            } else {
              console.log(`👎  ${testType} test failed`.red)
              throw new Error('Test failed.')
            }
          }
        })
      } else if (typeof rslt === 'object') {
        if ('css' in rslt) {
          if (xpctd === rslt.css) {
            console.log(`👍  ${testType} test passed`.green)
          } else {
            console.log(`👎  ${testType} test failed`.red)
            throw new Error('Test failed.')
          }
        }
      }
    }
  })
}


sass.render({
  file: file,
  outputFormat: 'expanded',
  outFile: outFile
}, (err, result) => {
  if (err) {
    throw err
    } else {
      fs.writeFile(outFile, result.css, err => {
        if (err) {
          throw err
        } else {
          xPect('SCSS', expected, outFile)
        }
      })
    }
});


fs.readFile(filePost, 'utf-8', (err,result)=> {
  if (err) {
    throw err
  } else {
  postcss()
    .use(gIndex({
      levels: {
        below: -1,
        zero: 0,
        above: 1
      }
    }))
    .process(result, { from: filePost, to: outFilePost })
    .then((result)=> {
      console.log('result')
      fs.writeFile(outFilePost, result.css)
      xPect('postcss', expectedPost, result)
    }).catch(err => {
      throw err
    })
  }
})
